.PHONY: all ex*

all: ex1 ex2 ex3 ex4 ex5 ex6

ex1:
	make -C ./exemple1

ex2:
	make -C ./exemple2

ex3:
	make -C ./exemple3

ex4:
	make -C ./exemple4

ex5:
	make -C ./exemple5

ex6:
	make -C ./exemple6
