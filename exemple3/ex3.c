#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

void shell(){
	setreuid(geteuid(), geteuid());
	system("/bin/sh");
}

int main(){
	char name[10];
	
	printf("Name: ");
	gets(name);

	printf("Hello %s\n", name);
}
